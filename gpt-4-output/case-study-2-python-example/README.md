# Python Case Study #2

## Prompt 1
**Reaction:** 
_“To a solution of 4-fluorobenzaldehyde (20.0 mmol, 1.0 equiv.) in methanol (20 mL) was added hydrazine hydrate (1.2 mL, 24 mmol, 1.2 equiv.) and the mixture stirred at r.t. for 1 h.
”_


**Instruction:** Help me understand this reaction, and break it into phases if this is really long. After you identified the step, if the mass or volume is not given, please help me with calculating the mass/volume used for each compound. You can look up the molecular weight too. help me rewrite this procedure to a detailed stepwise procedure with the actual mass/volume to be added. Each step should only have one chemical being added.


## Prompt 2
Now, following the steps above strictly, write a complete procedure using the template below, 

```python
kinova_shaker = Shaker(name='kinova shaker')
europa = EuropaHandling(name='europa’)


acetone = Liquid(name='acetone', formula='C3H6O')
salt = Solid(name='salt', formula='NaCl')

# vial tray vials
vial_1 = Vial(name='vial 1')
vial_2 = Vial(name='vial 2')

# solvent tray vials
acetone_solvent_vial = Vial(name='acetone solvent', chemical=acetone)


# script action here
# only new liquids, solids and solvent tray vials can be added


add_solvent(europa, acetone_solvent_vial, vial_1, 10)
```

add solid reagents with the function add_reagent(reagent: Solid, vial_to_dose_to: Vial,  amount: float, units: str);

add solvent with the function add_solvent(handler: EuropaHandling, solvent_vial: Vial, vial_to_dispense_to: Vial, volume_in_ml: float), ignore this if there is no solvent; 

start to heat and stirring with the function heat_and_stir(shaker: Shaker, vial: Vial, duration: int, temperature: float, stir_rate: int);


## Procedure source citation
[1] Poh, J.; Tran, D. N.; Battilocchio, C.; Hawkins, J. M.; Ley, S. V. A Versatile Room‐Temperature Route to Di‐ and Trisubstituted Allenes Using Flow‐Generated Diazo Compounds. Angew. Chem. Int. Ed. 2015, 54 (27), 7920–7923. https://doi.org/10.1002/anie.201501538.


