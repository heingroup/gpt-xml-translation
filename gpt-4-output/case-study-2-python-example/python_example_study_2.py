from typing import Union


class EuropaHandling:
    def __init__(self, name: str):
        pass


class Shaker:
    def __init__(self, name: str):
        pass


class Liquid:
    def __init__(self, name: str, formula: str):
        pass


class Solid:
    def __init__(self, name: str, formula: str):
        pass


class Vial:
    def __init__(self, name: str, chemical: Union[Liquid, Solid] = None, grid_index: str = "A1"):
        pass


def add_solvent(handler: EuropaHandling, solvent_vial: Vial, vial_to_dispense_to: Vial, volume_in_ml: float):
    pass


def add_reagent(reagent: Solid, vial_to_dose_to: Vial, amount: float, units: str):
    pass


def heat_and_stir(shaker: Shaker, vial: Vial, duration: int, temperature: float, stir_rate: int):
    pass


"""
Already defined
"""
kinova_shaker = Shaker(name='kinova shaker')
europa = EuropaHandling(name='europa')
vial_1 = Vial(name='vial 1')
vial_2 = Vial(name='vial 2')

"""
there are already objects declared that are not included in this script, that would be needed to run the script, a
subset of the objects are defined in Prompt #2
script actions and new items ChatGPT generated
"""

"""
equipment has an error with the wrong quote used in europa, but this object is already defined above so we 
don't need the ChatGPT output

kinova_shaker = Shaker(name='kinova shaker')
europa = EuropaHandling(name='europa’)
"""

methanol = Liquid(name='methanol', formula='CH3OH')
fluorobenzaldehyde = Solid(name='4-fluorobenzaldehyde', formula='C7H5FO')
hydrazine_hydrate = Liquid(name='hydrazine hydrate', formula='N2H4·H2O')

# solvent tray vials: the grid indexes have been manually added
methanol_solvent_vial = Vial(name='methanol solvent', chemical=methanol, grid_index="A1")
hydrazine_solvent_vial = Vial(name='hydrazine solvent', chemical=hydrazine_hydrate, grid_index="A2")

# script action here

# Step 1: Add methanol solvent to vial 1
add_solvent(europa, methanol_solvent_vial, vial_1, 20)

# Step 2: Add 4-fluorobenzaldehyde to the methanol in vial 1
add_reagent(fluorobenzaldehyde, vial_1, 2.4822, 'g')

# Step 3: Add hydrazine hydrate to the reaction mixture in vial 1
add_solvent(europa, hydrazine_solvent_vial, vial_1, 1.2)

# Step 4: Stir the reaction mixture at room temperature for 1 hour
heat_and_stir(kinova_shaker, vial_1, duration=60, temperature=25, stir_rate=300)
