# Case Study #3

## prompt 1

Reaction: _"Once the system was configured, the system was tested with the Curtius rearrangement process previously highlighted and with conditions analogously described in SI section 2.d. Therefore, 10.00 mL of toluene was added to a scintillation vial equipped with a magnetic stir bar and septum filled cap (with venting needle). With the stopped-flow system pumping fresh toluene (collecting into waste), the ReactIR was blanked on toluene and these ‘in’ and ‘out’ lines were connected to the scintillation vial (adding 1.89 mL toluene to overall volume). Next, TEA (1.1 mL, 1.0 eq.) was added to the vial and allowed to circulate. With the ‘in’ line pulled out of solution, 7 (1.0361 g, 7.32 mmol, 1 eq.) was added. Once the solution appeared homogeneous, the ‘in’ line was again placed well below the liquid level. After, the NMR spectrometer and Python script were configured with experimental parameters, the stoppedflow script was started and IR spectra began to be collected every minute using the iC IR software (Mettler Toledo). Once the 5th 19F NMR spectrum started acquisition, DPPA (1.8 mL, 1.1 eq.) was added. Then, once the 15th spectrum started to be acquired, the reaction solution was lowered to a pre-heated oil bath (80 °C) as significant conversion of 7 to the corresponding 10 was observed. 12 (0.84 mL, 1.1 eq.) was added to the reaction solution once the 135th spectrum started to be acquired as significant conversion from the 10 to the corresponding isocyanate (11) was observed. The reaction was then monitored for the remaining time points as denoted with the script. For this trial, 19F NMR spectra were acquired with four transients, zero dummy scans, 90° pulse, 44 dB rg, 61 s d1, -90.0 ppm o1p, 219.6 ppm SW, and 2.79 s AQ (34,816 complex data points). In the Python script, 170 timepoints were collected every 510 s, with a 15 s pause time, 1.78 mL/min calibrated pump rate, and an additional t=0 s spectrum collected."_

Instruction: Help me understand this reaction, and break it to phases if this is really long. After you identified the step, if the mass or volume is not given, please help me with calculating the mass/volume used for each compound. You can look up the molecular weight too. help me rewrite this procedure to detailed stepwise procedure with the actual mass/volume to be added.

## additional ask
can you estimate the reaction time, according to the number of NMR spectrum, and the sampling interval, and update the procedure.

## prompt 2
prompt 2 does not change, 
[prompt_2.md](..%2F..%2Fprompt_2.md)

## Procedure source citation
[1] T. Maschmeyer, L. P. E. Yunker and J. E. Hein, Quantitative and convenient real-time reaction monitoring using stopped-flow benchtop NMR, React. Chem. Eng., 2022, 7, 1061–1072.
