# Generate iControl XML using Python functions

This python code example demonstrates how to generate XML files through Python function calls. 
This approach allows users to bypass the need for providing XML templates directly to ChatGPT, 
facilitating the creation of XML configurations for more complex procedures. 

## Modified Prompt 2
Now, following the steps above strictly, write a complete procedure using the template below,
```Python
class ExampleXmlGen:
    def dose_at_rate(self, chemical_formula: str, amount_in_ml: float, rate_ml_per_min: float = 5): ...
    def add_at_once(self, chemical_formula: str, amount: float, unit: str = "ml"): ...
    def stir(self, rate: str = 400, ramping_duration: float = 10): ...
    def heat_cool(self, temperature:float, ramping_duration_in_second: float = 10): ...
    def wait(self, duration_in_minute: float): ...

if __name__ == "__main__": 
    gen = ExampleXmlGen() 
    # script action here 
    gen.dose_at_rate("methanol", 10) 
```

add reagents with add_at_once(self, chemical_formula: str, amount: float, unit: str = "ml")
add solvent if there is any using dose_at_rate(self, chemical_formula: str, amount_in_ml: float, rate_ml_per_min: float = 5), ignore this if there is no solvent;
set temperature with heat_cool(self, temperature:float, ramping_duration_in_second: float = 10) and stir rate with stir(self, rate: str = 400, ramping_duration: float = 10)
set reaction wait time using wait(self, duration_in_minute: float)


## GPT-4 output
```python    
gen = ExampleXmlGen()
    
# Add solvent (methanol)
gen.dose_at_rate("methanol", 20, rate_ml_per_min=5)

# Add 4-fluorobenzaldehyde
gen.add_at_once("4-fluorobenzaldehyde", 2.4822, unit="g")

# Stir to dissolve
gen.stir(rate=400, ramping_duration=10)

# Add hydrazine hydrate
gen.add_at_once("hydrazine hydrate", 1.2, unit="ml")

# Stir the mixture
gen.stir(rate=400, ramping_duration=10)

# Wait for 1 hour
gen.wait(duration_in_minute=60)
```

## XML generation using `ExampleXmlGen`
After executing the above Python code, use `generate_xml` to output the iControl XML file
```python    
gen.generate_xml('example_xml_gen.xml')
```

The generated XML: [example_xml_gen.py](example_xml_gen.py)
