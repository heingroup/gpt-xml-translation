import inspect
from uuid import uuid4
from functools import wraps


# Step 1: Create the decorator
def log_function_call(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        # Get the function name
        function_name = func.__name__

        # Get the arguments passed to the function
        signature = inspect.signature(func)
        bound_arguments = signature.bind(*args, **kwargs)
        bound_arguments.apply_defaults()

        # Exclude 'self' if it's a method
        all_args = {k: v for k, v in bound_arguments.arguments.items() if k != 'self'}

        # Call the original function
        result = func(*args, **kwargs)

        # Access the instance (`self`) and log the function call and arguments
        instance = args[0]
        if isinstance(instance, ExampleXmlGen):
            if not function_name.startswith('_'):
                instance.operations.append(dict(action=function_name, kwargs=all_args))
            if 'chemical_formula' in all_args and all_args['chemical_formula'] not in instance.chemicals:
                instance.chemicals[all_args['chemical_formula']] = uuid4()

        return result

    return wrapper


# Step 2: Create the metaclass
class LogMethodsMeta(type):
    def __new__(cls, name, bases, dct):
        for attr_name, attr_value in dct.items():
            if callable(attr_value) and not attr_name.startswith('__'):
                dct[attr_name] = log_function_call(attr_value)
        return super().__new__(cls, name, bases, dct)


# Step 3: Define the class using the metaclass
class ExampleXmlGen(metaclass=LogMethodsMeta):
    def __init__(self):
        self.operations = []
        self.chemicals = {}

    def generate_xml(self, output_name=None):
        operations_xml = [self._generate_operation_xml(op) for op in self.operations]
        chemicals_xml = self._generate_chemicals_xml()
        xml_string = f"""{self._header()}{''.join(operations_xml)}{self._footer()}{chemicals_xml}"""
        with open('example_xml_gen.xml' or output_name, 'w') as f:
            f.write(xml_string)
        return xml_string

    def _generate_operation_xml(self, operation):
        func = getattr(self, f"_{operation['action']}")
        return func(**operation['kwargs'])

    def dose_at_rate(self, chemical_formula: str, amount_in_ml: float, rate_ml_per_min: float = 5):
        pass

    def add_at_once(self, chemical_formula: str, amount: float, unit: str = "ml"):
        pass

    def stir(self, rate: str = 400, ramping_duration: float = 10):
        pass

    def heat_cool(self, temperature:float, ramping_duration_in_second: float = 10):
        pass

    def wait(self, duration_in_minute: float):
        pass

    def _dose_at_rate(self, chemical_formula: str, amount_in_ml: float, rate_ml_per_min: float = 5):
        return f"""
                <DoseAtRateOperation TrackingId="{uuid4()}">
                  <DesignValues>
                    <RequiresUserInteraction>true</RequiresUserInteraction>
                    <Chemical TrackingId="{self.chemicals[chemical_formula]}" />
                    <AutomaticParameterSet>
                      <Amount Value="{amount_in_ml}" Unit="ml" />
                      <Rate Value="{rate_ml_per_min}" Unit="ml/min" />
                    </AutomaticParameterSet>
                  </DesignValues>
                </DoseAtRateOperation>"""

    def _add_at_once(self, chemical_formula: str, amount: float, unit: str = "ml"):
        return f"""
                <AddAtOnceOperation TrackingId="{uuid4()}">
                  <DesignValues>
                    <RequiresUserInteraction>true</RequiresUserInteraction>
                    <Chemical TrackingId="{self.chemicals[chemical_formula]}" />
                    <Amount Value="{amount}" Unit="{unit}" />
                  </DesignValues>
                </AddAtOnceOperation>"""

    def _stir(self, rate: str = 400, ramping_duration: float = 10):
        return f"""
                <StirOperation TrackingId="{uuid4()}">
                  <DesignValues>
                    <Duration Value="{ramping_duration}" Unit="sec" />
                    <EndValue Value="{rate}" Unit="rpm" />
                  </DesignValues>
                </StirOperation>"""

    def _heat_cool(self, temperature: float, ramping_duration_in_second: float = 10):
        return f"""                
                <HeatCoolOperation TrackingId="{uuid4()}">
                  <DesignValues>
                    <RampByDurationParameterSet TemperatureControlMode="Tr">
                      <EndValue Value="{temperature}" Unit="°C" />
                      <Duration Value="{ramping_duration_in_second}" Unit="sec" />
                    </RampByDurationParameterSet>
                  </DesignValues>
                </HeatCoolOperation>"""

    def _wait(self, duration_in_minute: float):
        return f"""
                <WaitOperation TrackingId="{uuid4()}">
                  <DesignValues>
                    <DurationParameterSet>
                      <Duration Value="{duration_in_minute}" Unit="min" />
                    </DurationParameterSet>
                  </DesignValues>
                </WaitOperation>"""

    # Create an instance of the class and call the methods with specific arguments

    def _header(self, exp_name: str = "example", user: str = "user", project: str = "project"):
        return f"""<Experiment
    UniqueElnId="{exp_name}"
    User="{user}"
    Project="{project}"
    SchemaVersion="3.0"
    TrackingId="{uuid4()}"
    p1:noNamespaceSchemaLocation=""
    xmlns:p1="http://www.w3.org/2001/XMLSchema-instance">
  <Process ProcessType="Synthesis" TrackingId="{uuid4()}">
    <Stages>
      <Stage StageType="Reaction" TrackingId="{uuid4()}">
        <Phases>
          <Phase TrackingId="{uuid4()}">
            <OperationSequences>
              <OperationSequence>"""

    def _footer(self):
        return f"""
                <EndExperimentOperation TrackingId="5b839654-dc17-47d0-b073-9c6c8aabc291">
                  <DesignValues>
                    <EndConditions>
                      <EndExperimentThermostatState>OffState</EndExperimentThermostatState>
                      <EndExperimentStirrerState>OffState</EndExperimentStirrerState>
                    </EndConditions>
                  </DesignValues>
                </EndExperimentOperation>
              </OperationSequence>
            </OperationSequences>
          </Phase>
        </Phases>
      </Stage>
    </Stages>
  </Process>"""

    def _generate_chemicals_xml(self):
        chemicals_xml = [
            f"""
    <Chemical Name="{chemical}" LotNumber="{chemical}" TrackingId="{chemical_id}">
      <VolumeAmount Value="1000" Unit="ml">
        <Density Value="1" Unit="g/ml" />
      </VolumeAmount>
    </Chemical>"""
            for chemical, chemical_id in self.chemicals.items()
        ]
        return f"""
  <Chemicals>{''.join(chemicals_xml)}
  </Chemicals>
  <Substances />
</Experiment>"""


if __name__ == "__main__":
    gen = ExampleXmlGen()
    gen.dose_at_rate("methanol", 20, rate_ml_per_min=5)

    # Add 4-fluorobenzaldehyde
    gen.add_at_once("4-fluorobenzaldehyde", 2.4822, unit="g")

    # Stir to dissolve
    gen.stir(rate=400, ramping_duration=10)

    # Add hydrazine hydrate
    gen.add_at_once("hydrazine hydrate", 1.2, unit="ml")

    # Stir the mixture
    gen.stir(rate=400, ramping_duration=10)

    # Wait for 1 hour
    gen.wait(duration_in_minute=60)

    xml = gen.generate_xml()
    print(xml)
